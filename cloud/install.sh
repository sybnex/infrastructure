#!/bin/bash

echo "INFO: creating secrets"
kubectl config use cloud

main() {
  # minio
  renovate
  yadda
  # matrix
  woodpecker
  # headlamp
  echo "INFO: all jobs are done"
  echo "Snippets:"
  echo "  kubectl -o yaml --dry-run=client -n kwatch create secret generic kwatch --from-file config.yaml | kubeseal -o yaml"
}

minio() {
  if [[ ! "$(kubectl -n minio-system get secrets -o name)" =~ "minio-secret" ]]; then
    echo -n "Enter password for minio auth: "
    read -rs MINIO_PASS && echo
    kubectl -n minio-system create secret generic minio-secret \
	        --from-literal=user_key=sybex \
	        --from-literal=pass_key="$MINIO_PASS"
    kubectl -n minio-system delete po -l app=minio
  fi
}

renovate() {
  if [[ ! "$(kubectl -n renovate get secrets -o name)" =~ "renovate-secret" ]]; then
    echo -n "taking token from env GITHUB_TOKEN and GITCBG_TOKEN!"
    kubectl -n renovate create secret generic renovate-secret \
            --from-literal=RENOVATE_AUTODISCOVER="false" \
            --from-literal=RENOVATE_ENDPOINT="https://codeberg.org/api/v1" \
            --from-literal=RENOVATE_GIT_AUTHOR="Renovate Bot <bot@yadda.ch>" \
            --from-literal=RENOVATE_PLATFORM="gitea" \
            --from-literal=RENOVATE_TOKEN="$GITCBG_TOKEN" \
            --from-literal=GITHUB_COM_TOKEN="$GITHUB_TOKEN"
  fi
}

yadda() {
  if [[ ! "$(kubectl -n yadda get secrets -o name)" =~ "mariadb-pass" ]]; then
    YADDA_PASS=$(openssl rand -hex 10)
    echo "Password created: $YADDA_PASS"
    kubectl -n yadda create secret generic mariadb-pass \
            --from-literal=password="$YADDA_PASS"
  fi
}

matrix() {
  if [[ ! "$(kubectl -n matrix get cm -o name)" =~ "matrix-config" ]]; then
    DIR=$(mktemp -d); mkdir "$DIR/data"; mkdir "$DIR/config"
    docker run -it --rm \
      -v "$DIR/data:/data" -v "$DIR/config:/config" \
      -e SYNAPSE_SERVER_NAME=yadda.ch \
      -e SYNAPSE_REPORT_STATS=no \
      -e SYNAPSE_CONFIG_DIR=/config \
      matrixdotorg/synapse:latest generate
    sudo chmod 777 "$DIR" ; tree "$DIR"/
    kubectl -n matrix create configmap matrix-config \
            --from-file "$DIR/config/homeserver.yaml" \
            --from-file "$DIR/config/yadda.ch.log.config" \
            --from-file "$DIR/config/yadda.ch.signing.key"
  fi
}

woodpecker() {
  NAMESPACE="woodpecker"
  if [[ ! "$(kubectl -n $NAMESPACE get secrets -o name)" =~ "woodpecker-secret" ]]; then
    WOODPECKER_SECRET=$(openssl rand -hex 32)
    echo "$NAMESPACE secret created: $WOODPECKER_SECRET"
    echo -n "Enter git client for $NAMESPACE auth: " && read -r GIT_CLIENT && echo
    echo -n "Enter git secret for $NAMESPACE auth: " && read -r GIT_SECRET && echo
    kubectl -n "$NAMESPACE" create secret generic "$NAMESPACE"-secret \
            --from-literal=WOODPECKER_AGENT_SECRET="$WOODPECKER_SECRET" \
            --from-literal=WOODPECKER_GITEA_CLIENT="$GIT_CLIENT" \
            --from-literal=WOODPECKER_GITEA_SECRET="$GIT_SECRET"
  fi

  if [[ ! "$(kubectl -n $NAMESPACE get secrets -o name)" =~ "dockerreg" ]]; then
    kubectl -n woodpecker create secret generic dockerreg --from-file ~/.docker/config.json
  fi
}

main

#!/bin/bash

if [ -z "$1" ]; then
  echo "usage $0 start/stop/restart"
  exit 1
fi

NAME="haproxy"
IMAGE="arm32v7/haproxy:alpine"

start() {
  docker run --restart always -d --name $NAME \
	  -v /home/pi/infrastructure/proxy/haconfig:/usr/local/etc/haproxy:ro \
	  -p 80:80 -p 443:443 -p 8404:8404 $IMAGE
  docker ps
}

stop() {
  docker stop $NAME
  docker rm   $NAME
  docker rmi  $IMAGE
}

restart() {
  stop
  start
}

if [ "$1" == "stop" ]; then stop; fi
if [ "$1" == "start" ]; then start; fi
if [ "$1" == "restart" ]; then restart; fi

exit 0

#!/bin/bash

if [ -z "$1" ]; then
  echo "usage $0 start/stop/restart"
  exit 1
fi

NAME="pihole"
IMAGE="pihole/pihole"
TAG=$(curl -s https://api.github.com/repos/pi-hole/docker-pi-hole/releases/latest | jq -r .tag_name)

start() {
  read -rsp "Enter web password: " WEBPASSWORD

  test -d /home/sybex/etc-dnsmasq.d/ || mkdir /home/sybex/etc-dnsmasq.d/
  test -d /home/sybex/etc-pihole/    || mkdir /home/sybex/etc-pihole/

  docker run --restart unless-stopped -d --name "$NAME" \
    -v /home/sybex/etc-pihole/:/etc/pihole/ \
    -v /home/sybex/etc-dnsmasq.d/:/etc/dnsmasq.d/ \
    -p 53:53/tcp -p 53:53/udp -p 67:67/udp \
    -p 8080:80/tcp \
    -e TZ="Europe/Zurich" \
    -e WEBPASSWORD="$WEBPASSWORD" \
    -e VIRTUAL_HOST="dns.julina.ch" \
    --net=host \
    "$IMAGE:$TAG"

  echo "Running with: $(docker inspect pihole | jq -r '.[0].Config.Env[]' | grep WEBPASSWORD)" 
}

stop() {
  docker stop "$NAME"
  docker rm   "$NAME"
}

restart() {
  docker pull "$IMAGE:$TAG"
  stop
  start
  docker image prune --force --all
}

if [ "$1" == "stop" ]; then stop; fi
if [ "$1" == "start" ]; then start; fi
if [ "$1" == "restart" ]; then restart; fi

exit 0

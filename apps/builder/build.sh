#!/bin/bash

set -e 

NAME="sybex/builder"

echo "INFO: building now $NAME"
docker build -t $NAME -f Dockerfile .

docker push $NAME

#!/bin/bash

set -e 

NAME="sybex/pico"

# shellcheck disable=SC2317
cleanup() {
  echo "INFO: cleanup"
  docker stop pico
  docker container prune -f
  docker image prune -f
}

trap cleanup EXIT
trap cleanup SIGTERM

echo "INFO: start local build"
DOCKER_BUILDKIT=1 docker build -t $NAME:test .

echo "INFO: testing image $NAME ..."
docker run -d --rm --name pico -p 8080:8080 $NAME:test
sleep 10
RESULT=$(curl -s localhost:8080)

if [[ "$RESULT" =~ "Congratulations, you have successfully installed" ]]; then
  figlet "success"
  exit 0
else
  figlet "error"
  echo "$RESULT"
  exit 1
fi

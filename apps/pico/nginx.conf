server {
  listen 8080;

  root /var/www/html;

  access_log /dev/stdout;
  error_log /dev/stderr;

  index index.php;

  location ~ \.(jpg|jpeg|gif|png|css|js|ico|svg|eot|ttf|woff|woff2|otf)$ {
    access_log off;
    expires    30d;
  }

  location ~ \.php$ {
    fastcgi_pass  127.0.0.1:9000;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    include       fastcgi_params;
  }

  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }

  location ~* /(\.git|cache|bin|logs|backup|tests)/.*$ { return 403; }
  location ~* /(system|vendor)/.*\.(txt|xml|md|html|json|yaml|yml|php|pl|py|cgi|twig|sh|bat)$ { return 403; }
  location ~* /user/.*\.(txt|md|json|yaml|yml|php|pl|py|cgi|twig|sh|bat)$ { return 403; }
  location ~ /(LICENSE\.txt|composer\.lock|composer\.json|nginx\.conf|web\.config|htaccess\.txt|\.htaccess) { return 403; }

  gzip on;
}

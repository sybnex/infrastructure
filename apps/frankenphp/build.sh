#!/bin/bash

set -e 

APP="frankenphp"
NAME="sybex/$APP"

# shellcheck disable=SC2317
cleanup() {
  echo "INFO: cleanup"
  docker stop "$APP"
  docker container prune -f
  docker image prune -f
}

trap cleanup EXIT
trap cleanup SIGTERM

hadolint Dockerfile --ignore DL3018

echo "INFO: start local build"
docker build --progress=plain -t $NAME:test .

echo "INFO: testing image $NAME ..."
docker run -d --rm --name "$APP" --build-arg ARCH="linux/amd64" -p 8080:80 $NAME:test
sleep 5
RESULT=$(curl -s localhost:8080)

if [[ "$RESULT" =~ "html" ]]; then
  figlet "success"
  exit 0
else
  figlet "error"
  echo "$RESULT"
  exit 1
fi

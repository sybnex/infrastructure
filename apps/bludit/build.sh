#!/bin/bash

# build bludit locally ...

set -e 

NAME="sybex/bludit"

# shellcheck disable=SC2317
cleanup() {
  echo "INFO: cleanup"
  docker stop bludit
  docker container prune -f
  docker image prune -f
}

trap cleanup EXIT
trap cleanup SIGTERM

test -f "$(which hadolint)" && hadolint Dockerfile --ignore DL3018 || echo

echo "INFO: start local build"
docker build --build-arg ARCH="amd64" --progress=plain -t $NAME:test .

echo "INFO: testing image $NAME ..."
docker run -d --rm --name bludit -p 8080:80 $NAME:test
sleep 5
RESULT=$(curl -s localhost:8080)
curl -sv -XPOST -d "username=admin&password=bludit78" "localhost:8080/install.php?language=en"                                                 ✔ 
curl -sv "localhost:8080/admin/"

if [[ "$RESULT" =~ "Install Bludit first" ]]; then
  figlet "success"
  exit 0
else
  figlet "error"
  echo "$RESULT"
  exit 1
fi

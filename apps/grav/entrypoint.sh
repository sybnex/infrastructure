#!/bin/bash

# extracts grav.zip if empty dir and start frankenphp

echo "$(date): Checking dir $(pwd)"
if [ -f "index.php" ]; then
    echo "$(date): Found grav installation version: $(head -1 CHANGELOG.md)"
elif [ -f "/app/grav.zip" ]; then
    cd /app || exit 1
    unzip -qo grav.zip
    shopt -s dotglob
    mv grav-admin/* -t public/
    rm -rf grav-admin/
    echo "$(date): Installed grav version: $(head -1 public/CHANGELOG.md)"
    PASS=$(pwgen 12)
    HASH=$(htpasswd -nbBC 10 hashed_password "${PASS}")
    {
        echo "state: enabled"
        echo "title: admin"
        echo "access:"
        echo "  admin:"
        echo "    login: true"
        echo "    super: true"
        echo "  site:"
        echo "    login: true"
        echo "${HASH}"
    } > /app/public/user/accounts/admin.yaml
    sed -i "s/d:/d: /" /app/public/user/accounts/admin.yaml
    echo "$(date): Created admin user with password: $PASS"
else
    echo "$(date): Could not find any grav content!"
    exit 1
fi

cd /app/public || exit 1
exec frankenphp php-server

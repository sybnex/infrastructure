#!/bin/bash

# buildscript for grav

set -e 

APP="grav"
NAME="sybex/$APP"

# shellcheck disable=SC2317
cleanup() {
  echo "INFO: cleanup"
  docker stop "$APP"
  docker container prune -f
  docker image prune -f
}

trap cleanup EXIT
trap cleanup SIGTERM

test -f "$(which hadolint)" && hadolint Dockerfile --ignore DL3018 || echo hadolint not found

echo "INFO: start local build"
docker build --progress=plain --build-arg ARCH="amd64" -t $NAME:test .

echo "INFO: testing image $NAME ..."
docker run -d --rm --name $APP -p 8080:80 $NAME:test
sleep 5
RESULT=$(curl -s localhost:8080)

if [[ "$RESULT" =~ "installation successful" ]]; then
  figlet "success"
  exit 0
else
  figlet "error"
  echo "$RESULT"
  exit 1
fi

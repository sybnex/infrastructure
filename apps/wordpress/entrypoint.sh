#!/bin/bash

# mv content to destination folder and start frankenphp

echo "$(date): Checking dir $(pwd)"  # assume starting in /app

if [ -f "/app/public/index.php" ]; then
    echo "$(date): Found wordpress installation version: $(head -1 CHANGELOG.md)"
elif [ -d "/app/wordpress" ]; then
    cd /app || exit 1
    shopt -s dotglob
    test -d public || mkdir public
    cp -R wordpress/* public/
else
    echo "$(date): Could not find any wordpress content!"
    exit 1
fi

if [ "$USE_SQLITE" == "true" ]; then
    cd /app/public/ || exit 1
    mv wp-config-sample.php wp-config.php
    cp wp-content/plugins/sqlite-database-integration/db.copy wp-content/db.php && \
    sed -i "s+{SQLITE_IMPLEMENTATION_FOLDER_PATH}+$(pwd)/wp-content/plugins/sqlite-database-integration+g" wp-content/db.php && \
    sed -i "s+{SQLITE_PLUGIN}+sqlite-database-integration/load.php+g" wp-content/db.php && \
    mkdir wp-content/database && \
    touch wp-content/database/.ht.sqlite
fi

if [ "$FORCE_SSL" == "true" ]; then
    echo "define('FORCE_SSL_ADMIN', true);" >> /app/public/wp-config.php
fi

cd /app || exit 1
exec frankenphp run --config /app/Caddyfile

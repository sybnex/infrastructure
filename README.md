# private infrastructure setup

## architecture
[![](https://mermaid.ink/img/pako:eNptkLtuwzAMRX9F4JwsHY2iU7p1alctjERbgvWCJAYN4vx75MopjKKaSJwjXoI3UFETDDBlTEZ8fMog2hsdf7-I1-PxTSwmxnkRk6179EMwJXddxMxnyoEqlW40dfuabCJnAy1CWaU7XasNW49TYzqqmbLhcxd-2zXk6fzNyBTiBSttk5QhtV-SC-W-Y-Ji_gWoFJWynwwH8JQ9Wt3ucVt9CdWQJwlDKzWNyK5KkOHeVE665b9rW2OGoWamAyDX-HUN6tl352SxXdfDMKIrdH8ASIh8HQ?type=png)](https://mermaid.live/edit#pako:eNptkLtuwzAMRX9F4JwsHY2iU7p1alctjERbgvWCJAYN4vx75MopjKKaSJwjXoI3UFETDDBlTEZ8fMog2hsdf7-I1-PxTSwmxnkRk6179EMwJXddxMxnyoEqlW40dfuabCJnAy1CWaU7XasNW49TYzqqmbLhcxd-2zXk6fzNyBTiBSttk5QhtV-SC-W-Y-Ji_gWoFJWynwwH8JQ9Wt3ucVt9CdWQJwlDKzWNyK5KkOHeVE665b9rW2OGoWamAyDX-HUN6tl352SxXdfDMKIrdH8ASIh8HQ)

# setup
- Connect to all the nodes and prepare usage of ssh keys

## control-plane
```bash
ssh <control-plane>
sudo su -
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable traefik --disable servicelb --node-external-ip=<SERVER_EXTERNAL_IP> --flannel-backend=wireguard-native --flannel-external-ip" sh -
```

## add nodes
```bash
ssh <worker-node>
sudo su -
curl -sfL https://get.k3s.io | K3S_URL=https://<control-plane>:6443 K3S_TOKEN=<token-from-control-plane> --node-external-ip=<SERVER_EXTERNAL_IP>" sh -
```

## argocd
install argocd
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

create ssh key for argocd 
```bash
ssh-keygen -t ed25519 -C "argocd@yadda.ch"
kubectl -n argocd create secret generic argocd-sshkey \
    --from-file=sshPrivateKey=id_rsa \
    --from-literal=type="git" \
    --from-literal=url="git@gitlab.com:yadda-ch"
```

install app of apps
```bash
kubectl apply -n argocd bootstrap/project.yaml
```

# usage
## use kubeseal
Install kubeseal according to [kubeseal@github](https://github.com/bitnami-labs/sealed-secrets#linux)

Create an kubernetes secret and pipe it trough kubeseal or use an existing one, e.g.:

```bash
kubectl -n woodpecker get secret woodpecker-secret -o yaml | kubeseal -o yaml
```

## vpa

run this: https://github.com/kubernetes/autoscaler/blob/master/vertical-pod-autoscaler/pkg/admission-controller/gencerts.sh

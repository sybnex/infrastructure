#!/bin/bash

sudo docker stop kuma
sudo docker rm kuma
sudo docker run -d \
	--restart always \
	--name kuma \
	--volume "/volume1/docker/kuma:/app/data" \
	-p 3001:3001 \
	louislam/uptime-kuma:1.23.15-alpine

sudo docker ps


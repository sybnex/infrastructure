#!/bin/bash

IMAGE="minio/minio:latest"

sudo docker stop minio
sudo docker rm minio
sudo docker rmi $IMAGE
sudo docker run -d \
	--restart always \
    --name "minio" \
    -p 9000:9000 -p 9090:9090 \
    -v /volume1/docker/minio/data:/mnt/data \
    -v /volume1/docker/minio/config.env:/etc/config.env \
    -e "MINIO_CONFIG_ENV_FILE=/etc/config.env" \
    $IMAGE server --console-address ":9090"

sudo docker ps

#!/bin/bash

sudo docker stop woodpecker
sudo docker rm woodpecker
sudo docker run -d \
	--restart always \
	--name woodpecker \
	--volume "/var/run/docker.sock:/var/run/docker.sock" \
	--env WOODPECKER_SERVER="cicd.yadda.ch:30900" \
	--env WOODPECKER_AGENT_SECRET="" \
	woodpeckerci/woodpecker-agent:latest

sudo docker ps


#!/bin/bash

sudo docker run -d \
	--restart always \
	--name webcam \
	--volume "/volume1/photo/_webcam:/var/www/localhost/htdocs/img" \
	-p 8078:80 \
	--memory 200m \
	sybex/webcam

sudo docker ps

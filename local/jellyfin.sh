#!/bin/bash

sudo docker stop jellyfin
sudo docker rm jellyfin
sudo docker run -d \
	--restart always \
	--name jellyfin \
	--volume "/volume1/docker/jellyfin/config:/config" \
	--volume "/volume1/docker/jellyfin/cache:/cache" \
	--volume "/volume3/video2:/media:ro" \
	-p 8096:8096 \
	--device /dev/dri/renderD128 \
	--device /dev/dri/card0 \
	--memory 4g \
	jellyfin/jellyfin:10.10.3

sudo docker ps


#!/bin/bash

NAME="gitea"

test -d /volume1/docker/gitea || mkdir /volume1/docker/gitea

sudo docker stop $NAME
sudo docker rm $NAME
sudo docker run -d \
    --restart always \
    --name $NAME \
    --volume "/volume1/docker/gitea/data:/data" \
    --env USER_UID="1000" \
    --env USER_GID="1000" \
    -p 3000:3000 \
    -p 2222:2222 \
    gitea/gitea:latest-rootless

sudo docker ps

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import http.server
import socketserver

PORT = 8080
PAGE="""\
<html>
  <head>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <title>Alpenblick</title>
  </head>
  <body>
    <center>
      <h3>Alpenblick</h3>
      <img class="img-responsive" src="latest.jpg" width="1280" height="720">
    </center>
  </body>
</html>
"""

class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)

class StreamingServer(socketserver.ThreadingMixIn, http.server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True

server = StreamingServer(("", PORT), RequestHandler)
print("Server started at localhost:" + str(PORT))
server.serve_forever()

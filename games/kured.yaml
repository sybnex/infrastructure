---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: kured
rules:
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["get", "patch"]
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["list", "delete", "get"]
  - apiGroups: ["apps"]
    resources: ["daemonsets"]
    verbs: ["get"]
  - apiGroups: [""]
    resources: ["pods/eviction"]
    verbs: ["create"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kured
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kured
subjects:
  - kind: ServiceAccount
    name: kured
    namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: kube-system
  name: kured
rules:  # Allow kured to lock/unlock itself
  - apiGroups: ["apps"]
    resources: ["daemonsets"]
    resourceNames: ["kured"]
    verbs: ["update"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  namespace: kube-system
  name: kured
subjects:
  - kind: ServiceAccount
    namespace: kube-system
    name: kured
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kured
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: kured
  namespace: kube-system
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: kured             # Must match `--ds-name`
  namespace: kube-system  # Must match `--ds-namespace`
spec:
  selector:
    matchLabels:
      name: kured
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        name: kured
    spec:
      serviceAccountName: kured
      tolerations:
        - key: node-role.kubernetes.io/control-plane
          effect: NoSchedule
        - key: node-role.kubernetes.io/master
          effect: NoSchedule
      hostPID: true  # Facilitate entering the host mount namespace via init
      restartPolicy: Always
      containers:
        - name: kured
          image: ghcr.io/kubereboot/kured:1.13.0
          imagePullPolicy: IfNotPresent
          securityContext:
            privileged: true  # Give permission to nsenter /proc/1/ns/mnt
          env:
            - name: KURED_NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
          command:
            - /usr/bin/kured
            - --time-zone=Europe/Zurich
            - --start-time=2:00
            - --end-time=4:00
            - --reboot-sentinel=/var/run/reboot-required
            - --force-reboot=true
            - --drain-grace-period=30
            - --drain-timeout=60s
